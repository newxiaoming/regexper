FROM    registry.cn-shenzhen.aliyuncs.com/mynew5tt-base/node:v16.14.0-alpine3.15

LABEL description="构建正则表达式的镜像"
LABEL maintainer="chn_614si@163.com"

## BUILD_PRODUCT_ENV 为打包构建的环境
## 没有用到的变量，请直接置空，不要删除

ARG BASE_DIR="/opt/websrv" \
    BUILD_PRODUCT_ENV="build" \
    SCHEME="" \
    PROXY_URL=""

ENV CONFIG_DIR="${BASE_DIR}/config"\
    BASE_PACKAGE="python3 python3-dev make autoconf g++"\
    WWWROOT_DIR="${BASE_DIR}/data/wwwroot"

WORKDIR /tmp

RUN sed -i 's/mirrors.aliyun.com/mirrors.ustc.edu.cn/g' /etc/apk/repositories &&\
    apk update && apk add ${BASE_PACKAGE} &&\
    mkdir -p ${WWWROOT_DIR} ${BASE_DIR}/tmp &&\
    rm -rf /var/cache/apk/* &&\
    rm -rf /tmp/* && ln -s /usr/local/bin/python3 /usr/local/bin/python

COPY package.json ${WWWROOT_DIR}/

WORKDIR ${WWWROOT_DIR}

RUN \
    npm install -g cnpm --registry=https://registry.npmmirror.com &&\
    # yarn config set registry https://registry.npmmirror.com &&\
    # yarn config set registry https://registry.npmmirror.com &&\
    npm config set disturl https://npmmirror.com/mirrors/node &&\
    npm config set python_mirror https://mirrors.aliyun.com/pypi/simple/ &&\
    # npm config set sass_binary_site https://repo.huaweicloud.com/node-sass &&\
    cnpm install

COPY . ${WWWROOT_DIR}

RUN \
    cnpm run ${BUILD_PRODUCT_ENV}

## 第二阶段构建
FROM    registry.cn-shenzhen.aliyuncs.com/mynew5tt-base/nginx:v1.20.2-alpine3.13

LABEL description="运行"
LABEL maintainer="chn_614si@163.com"

ARG BASE_DIR="/opt/websrv"

ENV \
    CONFIG_DIR="${BASE_DIR}/config"\
    WWWROOT_DIR="${BASE_DIR}/data/wwwroot"

WORKDIR /tmp

COPY --from=0 ${WWWROOT_DIR}/deploy/nginx/conf/ ${CONFIG_DIR}/nginx/conf.d/

RUN \
    mkdir -p ${BASE_DIR}/logs/nginx ${BASE_DIR}/tmp ${CONFIG_DIR}/nginx/certs.d &&\
    rm -rf /var/cache/apk/* &&\
    rm -rf /tmp/*

WORKDIR /tmp

COPY --from=0 ${WWWROOT_DIR}/build/ ${WWWROOT_DIR}

VOLUME ["${CONFIG_DIR}/nginx/conf.d", "${CONFIG_DIR}/nginx/certs.d", "${BASE_DIR}/logs", "${WWWROOT_DIR}", "${BASE_DIR}/tmp"]

EXPOSE 80 443

CMD ["nginx", "-g", "daemon off;"]